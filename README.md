# Emissão de Carimbo do Tempo com Autenticação Básica

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia PHP para emissão de carimbo do tempo utilizando autenticação básica e requisição conforme especificação da RFC 3161. 

### Tech

O exemplo utiliza as bibliotecas PHP abaixo:
* [PHP 7.4] - PHP is a popular general-purpose scripting language that is especially suited to web development
* [CURL] - Client URL Library
* [OpenSSL] - OpenSSL is a general-purpose cryptography library.

### Código de referência
* [Timestamps in PHP(RFC 3161)] - Dealing with Trusted Timestamps in PHP (RFC 3161

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com uma credencial (client_id/client_secret) válida.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à emissão do carimbo do tempo.

| Variável | Descrição | Arquivo de Configuração |
| ------ | ------ | ------ |
| CLIENT_ID | Client id definido na credencial. | bry-carimbo-exemplo
| CLIENT_SECRET | Client secret definido na credencial. | bry-carimbo-exemplo

### Uso

Para execução da aplicação de exemplo é necessário ter o PHP 7.x instalado na sua máquina, além de instalar a biblioteca php-curl.

Comandos:

Instalar php-curl (linux):

    -apt-get install php-curl

Caso o seu sistema operacional seja outro, verifique sobre a instalação [aqui](https://www.php.net/manual/pt_BR/curl.requirements.php).

Executar programa:

    -php bry-carimbo-exemplo.php

   [PHP 7.4]: <https://www.php.net/releases/7_4_0.php>
   [CURL]: <https://www.php.net/manual/pt_BR/book.curl.php>
   [Timestamps in PHP(RFC 3161)]: <https://d-mueller.de/blog/dealing-with-trusted-timestamps-in-php-rfc-3161/>
   [OpenSSL]: <https://www.openssl.org/>