<?php
require_once ("trustedTimestamps.php");
$document = 'teste carimbo';
$documentHash = hash ( 'sha256', $document);
$urlCarimbadora = 'https://cloud.bry.com.br/carimbo-do-tempo/tsp';
$clientId = 'CLIENT_ID';
$clientSecret = 'CLIENT_SECRET';

try {
    
	$requestFilePath = TrustedTimestamps::createRequestfile ( $documentHash );
    $response = TrustedTimestamps::signRequestfile ( $requestFilePath, $urlCarimbadora, $clientId, $clientSecret );
	
	//Enviando requisição de carimbo do tempo
    $timestampDate = TrustedTimestamps::getTimestampFromAnswer ( $response ['response_string'] );

	} catch ( \Exception $e ) {
    
	$response = array();
    $response ['message'] = "Falha ao carimbar.";
    $response ['erro_info'] = $e->getMessage();
	
	}

var_dump($response);